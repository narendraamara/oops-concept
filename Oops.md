# Object-Oriented JavaScript

## What is OOP in programming and OOP concept in JavaScript

<p style='text-align: justify;'>Object-Oriented Programming (OOP) is a programming language model organized around "objects" rather than "actions" and data rather than logic. Historically, a program has been viewed as a logical procedure that takes input data, processes it, and produces output data. The programming challenge was seen as how to write the logic, not how to define the data.</p>

Here’s a list of concepts that are most often used when talking about Object-oriented programming (OOP):

-   Object, Property, and Method.
-   Class.
-   Encapsulation.
-   Abstraction.
-   Inheritance.
-   Polymorphism.

</br>

### Object, Property, and Method.
**Object Literal**
<p style='text-align: justify;'>Create a new object in JavaScript by setting its properties inside curly braces. Object literals property values can be any data types, like function literals, arrays, strings, numbers, or boolean.</p>

```
const book = {title: "Hippie",author: "Paulo Coelho",year: "2018"}
```
</br>

**Object Constructor**
<p style='text-align: justify;'>Object constructor is the same as a regular function. It will be called each time an object is created. We can use them with the new keyword. Object constructor is useful when we want to create multiple objects with the same properties and methods.</p>

```
const book = {title: "Hippie",author: "Paulo Coelho",year:"2018"}
const book1 = {title: "The Alchemist",author: "Paulo Coelho",year: "1988"}
```
</br>

### Class

<p style='text-align: justify;'>Class is not an object,Classes are special functions. You can define functions using function expressions and declarations and you can define classes that way as well. We can create the number of objects using the blueprint.</p>

<p style='text-align: justify;'>We can use the class keyword and the name of the class. The syntax is similar to that of Java.</p>

Class syntax is a nice way to use object-oriented programming and managing prototypes:
```
let Book = function(name) { 
   this.name = name
}
let newBook = function(name) {
   Book.call(this, name)
}
newBook.prototype = Object.create(Book.prototype);
const book1 = new newBook("The Alchemist");
```
This is using ES6 class syntax:
```
class Book {
   constructor(name) {
      this.name = name
   }
}
class newBook extends Book { 
   constructor(name) {
      super(name);
   }
}
const book1 = new newBook("The Alchemist");
```
</br>

### Encapsulation

<p style='text-align: justify;'>Encapsulation is a concept that allows the use of an object without having to know its internal implementation in detail.</p>


<p style='text-align: justify;'>The interaction with an object is done only via its public interface, which contains public members and methods.</p>

<p style='text-align: justify;'>We can say that encapsulation allows an object to be treated as a "black box", separating the implementation from its interface.</p>

<p style='text-align: justify;'>Think of the objects you've worked with so far:
document , a DOM object, and xmlHttp , an XMLHttpRequest object. You certainly don't know how these objects do their work internally! All you have to know is the
features you can use.
The "features you can use" of a class form the public interface of a class, which is the sum of all its public members. The public members are those members that are
visible and can be used by external classes. For example, the innerHTML property of a DOM object (such as the default document object), or the open() and send()
methods of XMLHttpRequest, are all public, because you were able to use them.
Each class can also contain private members, which are meant for internal usage only and aren't visible from outside.</p>

```
const Book = function(t, a) {
   let title = t; 
   let author = a; 
   
   return {
      summary : function() { 
        console.log(`${title} written by ${author}.`);
      } 
   }
}
const book1 = new Book('Hippie', 'Paulo Coelho');
book1.summary();
> Hippie written by Paulo Coelho.
```
</br>

### Abstraction

<p style='text-align: justify;'>Abstraction means implementation hiding. It is a way of hiding the implementation details and only showing the essential features to the caller. In other words, it hides irrelevant details and shows only what’s necessary to the outer world. A lack of abstraction will lead to problems of code maintainability.</p>

```
const Book = function(getTitle, getAuthor) { 
   // Private variables / properties  
   let title = getTitle; 
   let author = getAuthor;
// Public method 
   this.giveTitle = function() {
      return title;
   }
   
   // Private method
   const summary = function() {
      return `${title} written by ${author}.`
   }
// Public method that has access to private method.
   this.giveSummary = function() {
      return summary()
   } 
}
const book1 = new Book('Hippie', 'Paulo Coelho');
book1.giveTitle();
> "Hippie"
book1.summary();
> Uncaught TypeError: book1.summary is not a function
book1.giveSummary();
> "Hippie written by Paulo Coelho."
```
</br>

### Inheritance

<p style='text-align: justify;'>It is a concept in which some property and methods of an Object is being used by another Object. Unlike most of the OOP languages where classes inherit classes, JavaScript Object inherits Object.extends keyword is used in inheritance.</p>

<p style='text-align: justify;'>Using class inheritance, a class can inherit all the methods and properties of another class.</p>

Inheritance is a useful feature that allows code reusability.
```
function Book(title, author, year) { 
   this.title = title; 
   this.author = author; 
   this.year = year;
   this.summary = function() { 
      console.log(`${this.title} is written by ${this.author}.`)
   }
}
const book1 = new Book ('Hippie', 'Paulo Coelho', '2018');
const book2 = new Book ('The Alchemist', 'Paulo Coelho', '1988');
```
</br>

### Prototypal Inheritance

<p style='text-align: justify;'>For each instance of Book, we’re recreating the memory for the methods from the base class. These methods must be shared across all instances — they should not be specific to the instance. Here the prototype comes into the following,</p>

```
let Corebook = function(title) {
  this.title = title
}
Corebook.prototype.title = function() {
  console.log(`name of the book is ${this.title}`);
}
Corebook.prototype.summary = function(author) {
  console.log(`${this.title} is written by ${this.author}`);
}
let Book = function(title, author) {
  Corebook.call(this, title, author)
}
Book.prototype = Object.create(Corebook.prototype);
let book1 = new Book('The Alchemist', 'Paulo Coelho');
book1.title();
> name of the book is The Alchemist
book1.summary();
> The Alchemist is written by Paulo Coelho
```
</br>

### Polymorphism

<p style='text-align: justify;'>Polymorphism is a more advanced OOP feature that allows using objects of different classes when you only know a common base class from which they both derive.
Polymorphism permits using a base class reference to access objects of that class, or objects of derived classes. 
Using polymorphism, you can have, for example, a
method that receives as parameter an object of type Car, and when calling tha method you supply as parameter an object of type SuperCar.
Because SuperCar is a specialized version of Car , all the public functionality of Car would also be supported by SuperCar, although the SuperCar implementations could differ from those of Car.
This kind of fl exibility gives much power to an experienced programmer who knows how to take advantage of it.</p>

```
let book1 = function () {}
book1.prototype.summary = function() {
   return "summary of book1"
}
let book2 = function() {}
book2.prototype = Object.create(book1.prototype);
book2.prototype.summary = function() {                 
   return "summary of book2"
}
let book3 = function() {}
book3.prototype = Object.create(book1.prototype);
book3.prototype.summary = function() {
   return "summary of book3"
}
   
var books = [new book1(), new book2(), new book3()];
books.forEach(function(book){
   console.log(book.summary());
});
> summary of book1
> summary of book2
> summary of book3
```
</br>

## Prototype-based and Class-based OOP have their advantages and disadvantages
<p style='text-align: justify;'>In prototype based programming, there is no need to declare class as pre-planning about the sort of properties required before creating an object.Since no class needs to be made, you can create the object directly. This also offers flexibility; hence, any changes to the objects can easily and quickly be made while they’re being used.
While all these advantages exist in Prototype-based programming, there is a higher risk of incorrectness as abrupt changes can easily be made. Whereas in the Class-based approach, the blueprints layout a plan beforehand, decreasing the chances of bugs arising.</p></br>

## What is call stack?
<p style='text-align: justify;'>A call stack is a mechanism for an interpreter (like the JavaScript interpreter in a web browser) to keep track of its place in a script that calls multiple functions — what function is currently being run and what functions are called from within that function, etc.

   - When a script calls a function, the interpreter adds it to the call stack and then starts carrying out the function.

   - Any functions that are called by that function are added to the call stack further up, and run where their calls are reached.

   - When the current function is finished, the interpreter takes it off the stack and resumes execution where it left off in the last code listing.
   - If the stack takes up more space than it had assigned to it, it results in a “stack overflow” error.</p>



```
function greeting() {
   // [1] Some codes here
   sayHi();
   // [2] Some codes here
}
function sayHi() {
   return "Hi!";
}

// Invoke the `greeting` function

greeting();
```
</br>

## Higher Oder Function

<p style='text-align: justify;'>A function that accepts and/or returns another function is called a higher-order function.The Array.map method is one of the most common higher-order functions. We will use it as an example here. But there are several other higher-order functions: .filter, .reduce, etc.</p>

```
let array = [2,3,4]
function add_one(value ) {
   return value+1
}
const result = array.map(add_one);
console.log(result)  [ 3, 4, 5 ]

```

References :

https://betterprogramming.pub/object-oriented-programming-in-javascript-b3bda28d3e81

https://medium.com/swlh/intro-to-object-oriented-programming-in-javascript-fe90c70ab316